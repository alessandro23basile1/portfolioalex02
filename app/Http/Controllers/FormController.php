<?php

namespace App\Http\Controllers;
use App\Mail\ContattoDalForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\FormSiteRequest;

class FormController extends Controller
{
    public function send (FormSiteRequest $request){
        $data = [
            'name' =>$request->input('name'),
            'email' =>$request->input('email'),
            'project' =>$request->input('project'),
            'message' =>$request->input('message'),
        ];
        Mail::to($data['email'])->send(new ContattoDalForm($data));
        return redirect()->back()->with('status','Email inviata con successo');
       }
    }

