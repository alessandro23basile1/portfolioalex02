<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home(){
        return view('page.welcome');
    }

    public function about(){
        return view('page.chiSiamo');
    }
    public function project(){
        return view('page.progetti');
    }
    public function service(){
        return view('page.servizi');
    }
    public function contact(){
        return view('page.contatti');
    }

    public function promotions(){
        return view('page.promozioni');
    }
    public function energi(){
        return view('page.energiaRinnovabile');
    }
    public function modale(){
        return view('page.modale_cdz');
    }
}
