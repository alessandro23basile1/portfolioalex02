 <!-- Footer Start -->
 <div class="container-fluid footer bg-dark wow fadeIn" data-wow-delay=".3s">
    <div class="container pt-5 pb-4">
        <div class="row g-5">
            <div class="col-lg-3 col-md-6">
                <img class="img_footer" src="../../../img/AB logo.png" alt="ciao">
                <a href="{{route('home')}}">
                    <h1 class="text-white fw-bold d-block">AB Elettrical Solution </h1>
                </a>
                <p class="mt-4 text-light">Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta facere delectus qui placeat inventore consectetur repellendus optio debitis.</p>
                <div class="d-flex hightech-link">
                    <a href="" class="btn-light nav-fill btn btn-square rounded-circle me-2"><i class="fab fa-facebook-f text-primary"></i></a>
                    <a href="" class="btn-light nav-fill btn btn-square rounded-circle me-2"><i class="fab fa-twitter text-primary"></i></a>
                    <a href="" class="btn-light nav-fill btn btn-square rounded-circle me-2"><i class="fab fa-instagram text-primary"></i></a>
                    <a href="" class="btn-light nav-fill btn btn-square rounded-circle me-0"><i class="fab fa-linkedin-in text-primary"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <a href="#" class="h3 text-secondary">Short Link</a>
                <div class="mt-4 d-flex flex-column short-link">
                    <a href="{{route('home')}}" class="mb-2 text-white"><i class="fas fa-angle-right text-secondary me-2"></i>Home</a>
                    <a href="{{route('about')}}" class="mb-2 text-white"><i class="fas fa-angle-right text-secondary me-2"></i>Chi-Siamo</a>
                    <a href="{{route('service')}}" class="mb-2 text-white"><i class="fas fa-angle-right text-secondary me-2"></i>Servizi</a>
                    <a href="{{route('contact')}}" class="mb-2 text-white"><i class="fas fa-angle-right text-secondary me-2"></i>Contattaci</a>
                    <a href="{{route('project')}}" class="mb-2 text-white"><i class="fas fa-angle-right text-secondary me-2"></i>Progetti</a>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-6">
                <a href="#" class="h3 text-secondary">Contatti</a>
                <div class="text-white mt-4 d-flex flex-column contact-link">
                    <a href="#" class="pb-3 text-light border-bottom border-primary"><i class="fas fa-map-marker-alt text-secondary me-2"></i> Via dell'Olmata N°50 Nettuno (Rm) Italy.</a>
                    <a href="#" class="py-3 text-light border-bottom border-primary"><i class="fas fa-phone-alt text-secondary me-2"></i> +39-3703279036</a>
                    <a href="#" class="py-3 text-light border-bottom border-primary"><i class="fas fa-envelope text-secondary me-2"></i>abelettricalsolution@gmail.it</a>
                </div>
            </div>
        </div>
        <hr class="text-light mt-5 mb-4">
        <div class="row">
            <div class="col-md-6 text-center text-md-start">
                <span class="text-light"><a href="#" class="text-secondary"><i class="fas fa-copyright text-secondary me-2"></i>messaonlineab.abelettricalsolution.it</a>, All right reserved.</span>
            </div>
            {{-- <div class="col-md-6 text-center text-md-end">
                <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                <span class="text-light">Designed By<a href="https://htmlcodex.com" class="text-secondary">HTML Codex</a> Distributed By <a href="https://themewagon.com">ThemeWagon</a></span>
            </div> --}}
        </div>
    </div>
</div>

<!-- Footer End -->


        <!-- Back to Top -->
        <a href="{{route('home')}}" class="btn btn-secondary btn-square rounded-circle back-to-top"><i class="fa fa-arrow-up text-white"></i></a>

        