<!-- Navbar Start -->
<div class="container-fluid navbar_color">
    <div class="container">
        <nav class="navbar navbar-dark navbar-expand-lg py-0">
            <a href="{{ route('home') }}" class="navbar-brand">
                <h1 class=" brand_site  fw-bold d-block">AB Electric Solution </h1>
            </a>
            <button type="button" class="navbar-toggler me-0" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse bg-transparent" id="navbarCollapse">
                <div class="navbar-nav ms-auto mx-xl-auto p-0">
                    {{-- <a href="{{route('home')}}" class="nav_textcolore nav-item nav-link active ">Home</a> --}}
                    <a href="{{ route('home') }}" class="nav_textcolore nav-item nav-link">Home</a>
                    <a href="{{ route('about') }}" class="nav_textcolore nav-item nav-link">Chi Siamo</a>
                    <a href="{{ route('service') }}" class="nav_textcolore nav-item nav-link">I Nostri Servizi</a>
                    <a href="{{ route('project') }}" class="nav_textcolore nav-item nav-link">Progetti</a>
                    <a href="{{ route('contact') }}" class="nav_textcolore nav-item nav-link">Contatti</a>
                    <a href="{{ route('promotions') }}" class="nav_textcolore nav-item nav-link">Promozioni</a>

                </div>
                {{-- <a href="{{ route('contact') }}" class="mx-3 nav-item nav-link">Contact</a>
                <a href="{{ route('contact') }}" class="nav-item nav-link">Contact</a> --}}
            </div>
    </div>
    <div class="d-none d-xl-flex flex-shirink-0">
        <div id="phone-tada" class="d-flex align-items-center justify-content-center me-4">
            <a href="" class="position-relative animated tada infinite">
                <i class="fa fa-phone-alt text-white fa-2x"></i>
                <div class="position-absolute" style="top: -7px; left: 20px;">
                    <span><i class="fa fa-comment-dots text-secondary"></i></span>
                </div>
            </a>
        </div>
        <div class="d-flex flex-column pe-4 border-end">
            <span class="text-black">Hai una domanda chiamaci!</span>
            <span><a class="text_phone" href="">Tel: 370/3279036</a></span>
        </div>
        <div class="d-flex align-items-center justify-content-center ms-4 ">
            <a href="#"><i class="bi bi-search text-white fa-2x"></i> </a>
        </div>
    </div>
    </nav>
</div>
</div>
<!-- Navbar End -->
