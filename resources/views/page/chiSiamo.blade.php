<x-main-layout>
    <!-- About Start -->
    <div class="container-fluid py-5 my-5">
       <div class="container pt-5">
           <div class="row g-5">
               <div class="col-lg-5 col-md-6 col-sm-12 wow fadeIn" data-wow-delay=".3s">
                   <div class="h-100 position-relative">
                       <img src="img/about-1.jpg" class="img-fluid w-75 rounded" alt="" style="margin-bottom: 25%;">
                       <div class="position-absolute w-75" style="top: 25%; left: 25%;">
                           <img src="img/about-2.jpg" class="img-fluid w-100 rounded" alt="">
                       </div>
                   </div>
               </div>
               <div class="col-lg-7 col-md-6 col-sm-12 wow fadeIn" data-wow-delay=".5s">
                   <h5 class="text_chisiamo">Chi Siamo</h5>
                   <h1 class="mb-4">AB Elettrical Solution</h1>
                   <p>Una piccola impresa nata nel 2023 da un giovane imprenditore 
                    con esperienza nel settore da oltre 15 anni.</p>
                   <p class="mb-4">
                                           
                        Gestistiamo con passione e professionalità le 
                        manutenzioni elettriche <b>Civili ed Industriali.</b>
                        Con esperienza decennale nel settore condominiale, alberghiero e privato,
                        velocità d’esecuzione e cura dei dettagli  sono la nostra caratterista
                        principale.Ci occupiamo di gestione ordinaria, come “sostituzione
                        lampade, LED e neon” ma anche nella manutenzione citofonica,
                        antennistica e riparazione cancelli carrabili elettrici.
                        Prezzi vantaggiosi e qualità del servizio, oltre ad un’affidabilità
                        e gentilezza estrema sono il punto di forza sulla manutenzioni 
                        condominiali.
                    </p>
               </div>
           </div>
       </div>
   </div>
   <!-- About End -->
   </x-main-layout>