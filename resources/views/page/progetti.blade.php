<x-main-layout>
    <!-- Project Start -->
    <div class="container-fluid project py-5 mb-5">
        <div class="container">
            <div class="text-center mx-auto pb-5 wow fadeIn" data-wow-delay=".3s" style="max-width: 600px;">
                <h1 class="text-dark">I Nostri Progetti</h1>                
            </div>

{{-- inizio prova  --}}
<section class="wrapper">
    <div class="container">
      <div class="row">
        <div class="col text-center mb-5">
           <h1 class="display-4 font-weight-bolder">I Nostri Progetti</h1>
    <p class="lead">Lorem ipsum dolor sit amet at enim hac integer volutpat maecenas pulvinar. </p>
        </div>
      </div>
    <div class="row">
   <div class="col-sm-12 col-md-6 col-lg-4 mb-4"><div class="card text-dark card-has-bg click-col" style="background-image:url('/img/condizionatore.jpg');">
           <img class="card-img d-none" src="https://source.unsplash.com/600x900/?tech,street" alt="Creative Manner Design Lorem Ipsum Sit Amet Consectetur dipisi?">
          <div class="card-img-overlay d-flex flex-column">
           <div class="card-body">            
              <small class="card-meta mb-2">Occasione !!!!!!</small>
              <h4 class="card-title mt-0 "><h3 class="text-dark">Offerta Condizionatore</a></h4>
             <small><i class="far fa-clock"></i> 6 maggio 2024</small>
            </div>
            <div class="col">
                <p class="">Offerta del mese: 
                <br> montaggio più un condizionatore 9000 BTU SENDO più certficazione a SOLI</p>
                <p class="fs-2 text-center">750€ </p>
            </div>
            <div class="d-grid gap-2 d-md-block">
                <button  class="btn btn-sm btn-outline-dark"><a class="" href="{{route('modale')}}">Contattaci ora !!!</a> </button>
            
            
                <button class="btn btn-sm btn-outline-dark">Info</button>
            </div>
            {{-- <div class="img">
                <img class=" size_img_05" src="/img/condizionatore.jpg" alt="condizionatore">
            </div> --}}
            <div class="card-footer">
             <div class="media">
    
  </div>
            </div>
          </div>
        </div></div>
       <div class="col-sm-12 col-md-6 col-lg-4 mb-4"><div class="card text-dark card-has-bg click-col" style="background-image:url('https://source.unsplash.com/600x900/?tree,nature');">
           <img class="card-img d-none" src="https://source.unsplash.com/600x900/?tree,nature" alt="Creative Manner Design Lorem Ipsum Sit Amet Consectetur dipisi?">
          <div class="card-img-overlay d-flex flex-column">
           <div class="card-body">
              <small class="card-meta mb-2">Thought Leadership</small>
              <h4 class="card-title mt-0 "><a class="text-dark" herf="https://creativemanner.com">Creative Manner Lorem Ipsum Sit Amet Consectetur dipisi?</a></h4>
             <small><i class="far fa-clock"></i> October 15, 2020</small>
            </div>
            <div class="card-footer">
             <div class="media">
    <img class="mr-3 rounded-circle" src="https://assets.codepen.io/460692/internal/avatars/users/default.png?format=auto&version=1688931977&width=80&height=80" alt="Generic placeholder image" style="max-width:50px">
    <div class="media-body">
      <h6 class="my-0 text-dark d-block">Oz Coruhlu</h6>
       <small>Director of UI/UX</small>
    </div>
  </div>
            </div>
          </div>
        </div></div>
    <div class="col-sm-12 col-md-6 col-lg-4 mb-4"><div class="card text-dark card-has-bg click-col" style="background-image:url('https://source.unsplash.com/600x900/?computer,design');">
           <img class="card-img d-none" src="https://source.unsplash.com/600x900/?computer,design" alt="Creative Manner Design Lorem Ipsum Sit Amet Consectetur dipisi?">
          <div class="card-img-overlay d-flex flex-column">
           <div class="card-body">
              <small class="card-meta mb-2">Thought Leadership</small>
              <h4 class="card-title mt-0 "><a class="text-dark" herf="https://creativemanner.com">Design Studio Lorem Ipsum Sit Amet Consectetur dipisi?</a></h4>
             <small><i class="far fa-clock"></i> October 15, 2020</small>
            </div>
            <div class="card-footer">
             <div class="media">
    <img class="mr-3 rounded-circle" src="https://assets.codepen.io/460692/internal/avatars/users/default.png?format=auto&version=1688931977&width=80&height=80" alt="Generic placeholder image" style="max-width:50px">
    <div class="media-body">
      <h6 class="my-0 text-dark d-block">Oz Coruhlu</h6>
       <small>Director of UI/UX</small>
    </div>
  </div>
            </div>
          </div>
        </div></div>
     
      <div class="col-sm-12 col-md-6 col-lg-4 mb-4"><div class="card text-dark card-has-bg click-col" style="background-image:url('https://source.unsplash.com/600x900/?tech,street');">
           <img class="card-img d-none" src="https://source.unsplash.com/600x900/?tech,street" alt=" Lorem Ipsum Sit Amet Consectetur dipisi?">
          <div class="card-img-overlay d-flex flex-column">
           <div class="card-body">
              <small class="card-meta mb-2">Thought Leadership</small>
              <h4 class="card-title mt-0 "><a class="text-dark" herf="https://creativemanner.com">UI/UX Design Lorem Ipsum Sit Amet Consectetur dipisi?</a></h4>
             <small><i class="far fa-clock"></i> October 15, 2020</small>
            </div>
            <div class="card-footer">
             <div class="media">
    <img class="mr-3 rounded-circle" src="https://assets.codepen.io/460692/internal/avatars/users/default.png?format=auto&version=1688931977&width=80&height=80" alt="Generic placeholder image" style="max-width:50px">
    <div class="media-body">
      <h6 class="my-0 text-dark d-block">Oz Coruhlu</h6>
       <small>Director of UI/UX</small>
    </div>
  </div>
            </div>
          </div>
        </div></div>
       <div class="col-sm-12 col-md-6 col-lg-4 mb-4"><div class="card text-dark card-has-bg click-col" style="background-image:url('https://source.unsplash.com/600x900/?tree,nature');">
           <img class="card-img d-none" src="https://source.unsplash.com/600x900/?tree,nature" alt="Creative Manner Design Lorem Ipsum Sit Amet Consectetur dipisi?">
          <div class="card-img-overlay d-flex flex-column">
           <div class="card-body">
              <small class="card-meta mb-2">Thought Leadership</small>
              <h4 class="card-title mt-0 "><a class="text-dark" herf="https://creativemanner.com">Creative Manner Design Lorem Ipsum Sit Amet Consectetur dipisi?</a></h4>
             <small><i class="far fa-clock"></i> October 15, 2020</small>
            </div>
            <div class="card-footer">
             <div class="media">
    <img class="mr-3 rounded-circle" src="https://assets.codepen.io/460692/internal/avatars/users/default.png?format=auto&version=1688931977&width=80&height=80" alt="Generic placeholder image" style="max-width:50px">
    <div class="media-body">
      <h6 class="my-0 text-dark d-block">Oz Coruhlu</h6>
       <small>Director of UI/UX</small>
    </div>
  </div>
            </div>
          </div>
        </div></div>
    <div class="col-sm-12 col-md-6 col-lg-4 mb-4"><div class="card text-dark card-has-bg click-col" style="background-image:url('https://source.unsplash.com/600x900/?computer,design');">
           <img class="card-img d-none" src="https://source.unsplash.com/600x900/?computer,design" alt="Creative Manner Design Lorem Ipsum Sit Amet Consectetur dipisi?">
          <div class="card-img-overlay d-flex flex-column">
           <div class="card-body">
              <small class="card-meta mb-2">Thought Leadership</small>
              <h4 class="card-title mt-0 "><a class="text-dark" herf="https://creativemanner.com">Creative Manner Design Lorem Ipsum Sit Amet Consectetur dipisi?</a></h4>
             <small><i class="far fa-clock"></i> October 15, 2020</small>
            </div>
            <div class="card-footer">
             <div class="media">
    <img class="mr-3 rounded-circle" src="https://assets.codepen.io/460692/internal/avatars/users/default.png?format=auto&version=1688931977&width=80&height=80" alt="Generic placeholder image" style="max-width:50px">
    <div class="media-body">
      <h6 class="my-0 text-dark d-block">Oz Coruhlu</h6>
       <small>Director of UI/UX</small>
    </div>
  </div>
            </div>
          </div>
        </div></div>
    
  </div>
    
  </div>
  </section>
  
{{-- fine prova  --}}

            <div class="row g-5">

                {{-- prova card --}}
                {{-- <div class="col-md-6 col-lg-4">
                    <div class="card cardpersonal">
                        <img src="img/fotovoltaico01.jpeg" class=" imgpersonal_01" alt="...">
                        <div class="card-body">
                            <p class="pcard">
                                <a class="acard" href=""> Impianti Fotovoltaici</a>                               
                            </p>                                                                                        
                        </div>
                        
                    </div>
                </div>
                 --}}

                {{-- <div class="col-md-6 col-lg-4">
                    <div class="card cardpersonal">
                        <img src="img/impianti elettrici.png" class=" imgpersonal_02" alt="...">
                        <div class="card-body">
                            <p class="pcard">
                                Impianti Elettrici
                            </p>                             
                        </div>
                    </div>
                </div> --}}


                {{-- <div class="col-md-6 col-lg-4">
                    <div class="card cardpersonal">
                        <img src="/img/cond sendo.jpeg" class=" imgpersonal_02" alt="...">
                        <div class="card-body">
                            <p class="pcard">
                                Condizionamento
                            </p>                             
                        </div>
                    </div>
                </div>
                 --}}

              

            </div>
        </div>
    </div>
    <!-- Project End -->
</x-main-layout>
