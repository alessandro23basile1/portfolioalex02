<x-main-layout>
    <!-- Carousel Start -->
    <div class="container-fluid px-0">
        <div id="carouselId" class="carousel slide" data-bs-ride="carousel">

            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img src="/img/pannelli01.jpg" class="img-fluid" alt="First slide">
                    <div class="carousel-caption">
                        <div class="container carousel-content">
                            <h6 class="text-secondary h4 animated fadeInUp">AB Elettrical Solution</h6>
                            <h1 class="text-white display-1 mb-4 animated fadeInRight">Soluzioni con Energia Rinnovabile
                            </h1>
                            <p class="mb-4 text-white fs-5 animated fadeInDown">
                                Realizziamo impianti fotovoltaico su misura per la vostra abitazione.
                            </p>
                            <a href="{{ route('service') }}" class="me-2">
                                <button type="button"
                                    class="px-4 py-sm-3 px-sm-5 btn btn-primary rounded-pill carousel-content-btn1 animated fadeInLeft">
                                    Leggi di più...
                                </button>
                            </a>
                            <a href="{{route('contact')}}" class="ms-2">
                                <button type="button"
                                        class="px-4 py-sm-3 px-sm-5 btn btn-primary rounded-pill carousel-content-btn2 animated fadeInRight">
                                    Cotattaci
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="/img/carousel-2.jpg" class="img-fluid" alt="Second slide">
                    <div class="carousel-caption">
                        <div class="container carousel-content">
                            <h6 class="text-secondary h4 animated fadeInUp">AB Elettrical Solution</h6>
                            <h1 class="text-white display-1 mb-4 animated fadeInLeft">
                                Impianti elettrici civili,industriali e tecnologici
                            </h1>
                            <p class="mb-4 text-white fs-5 animated fadeInDown">
                                Proponiamo impianti di alta qualità con la massima
                                attenzione sulle esigenze dei nostri clienti
                            </p>
                            <a href="{{ route('service') }}" class="me-2">
                                <button type="button"
                                    class="px-4 py-sm-3 px-sm-5 btn btn-primary rounded-pill carousel-content-btn1 animated fadeInLeft">
                                    Leggi di più...
                                </button></a>
                            <a href="{{ route('contact') }}" class="ms-2">
                                <button type="button"
                                    class="px-4 py-sm-3 px-sm-5 btn btn-primary rounded-pill carousel-content-btn2 animated fadeInRight">
                                    Contattaci
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="/img/Condizionatore sendo.jpeg" class="img-fluid" alt="Second slide">
                    <div class="carousel-caption">
                        <div class="container carousel-content">
                            <h6 class="text-secondary h4 animated fadeInUp">AB Elettrical Solution</h6>
                            <h1 class="text-white display-1 mb-4 animated fadeInLeft">
                                Impianti di Condizionamento
                            </h1>
                            <p class="mb-4 text-white fs-5 animated fadeInDown">
                                Proponiamo impianti di alta qualità con la massima
                                attenzione sulle esigenze dei nostri clienti
                            </p>
                            <a href="{{ route('service') }}" class="me-2">
                                <button type="button"
                                    class="px-4 py-sm-3 px-sm-5 btn btn-primary rounded-pill carousel-content-btn1 animated fadeInLeft">
                                    Leggi di più...
                                </button></a>
                            <a href="{{ route('contact') }}" class="ms-2">
                                <button type="button"
                                    class="px-4 py-sm-3 px-sm-5 btn btn-primary rounded-pill carousel-content-btn2 animated fadeInRight">
                                    Contattaci
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselId" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselId" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div>
    <!-- Carousel End --> 

</x-main-layout>
