<x-main-layout>
    <!-- Services Start -->
    <div class="container-fluid services py-5 mb-5">
        <div class="container">
            <div class="text-center mx-auto pb-5 wow fadeIn" data-wow-delay=".3s" style="max-width: 600px;">
               
                <h1 class="h1servizi">Questi sono i nostri servizi che offriamo</h1>
            </div>
            <div class="row g-5 services-inner ">
                <div class="  col-md-6 col-lg-4 wow fadeIn" data-wow-delay=".3s">
                    <div class=" bg-light cardservizi ">
                        <div class="p-4 text-center services-content ">
                            <div class="services-content-icon">
                                <img class="img_servizi" src="\img\impianti-elettrici.png" alt="">
                                {{-- <i class="bi bi-plugin  fa fa-code fa-7x mb-4 text-primary icons-style"></i> --}}
                                <h4 class="mb-3 my-3">Impianti Elettrici</h4>
                                <p class="mb-4">
                                    Realizziamo <b>Impianti elettrici civili ed indrustriali</b>secondo le 
                                    normative vigenti,prestando massima attenzione alle esigenze dei
                                     nostri clienti.
                                </p>
                                                <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                                         data-bs-target="#impianti_elettrici">
                                                   Leggi di più...
                                                </button>                                        
                                                  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 wow fadeIn " data-wow-delay=".5s">
                    <div class=" bg-light cardservizi">
                        <div class="p-4 text-center services-content">
                            <div class="services-content-icon">
                                {{-- <i class="bi bi-fan fa fa-file-code fa-7x mb-4 text-primary"></i>                                 --}}
                                <img class="img_servizi_2" src="\img\condizionatore.jpg" alt="">
                                <h4 class="mb-3 my-3">Condizionamento</h4>
                                <p class="mb-4">
                                    Realizziamo <b>Impianti di condizionamento</b>  per abitazioni,
                                    a servizio di uno o più ambienti,mono e multi split, 
                                    per uso commerciale.
                                </p>
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                        data-bs-target="#condizionamento">
                                       Leggi di più...
                                </button>      
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 wow fadeIn" data-wow-delay=".7s">
                    <div class="cardservizi bg-light">
                        <div class="p-4 text-center services-content">
                            <div class="services-content-icon">
                                <img class="img_servizi_3" src="\img\safire.jpeg" alt="">
                                {{-- <i class="bi bi-camera-video fa fa-external-link-alt fa-7x mb-4 text-primary"></i> --}}
                                
                                <h4 class="mb-3 my-3">Impianti di video sorveglianza</h4>
                                <p class="mb-4">
                                    Realizziamo Impianti di videosorveglianza per rendere più
                                    sicure le vostre abitazioni e negozi commerciali.
                                </p>
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#condizionamento">
                                   Leggi di più...
                            </button>      
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 wow fadeIn" data-wow-delay=".7s">
                    <div class="cardservizi bg-light">
                        <div class="p-4 text-center services-content">
                            <div class="services-content-icon">
                              
                                <i class="bi bi-bell fa fa-external-link-alt fa-7x mb-4 text-primary"></i>
                                
                                <h4 class="mb-3">Impianti di antintrusione</h4>
                                <p class="mb-4">
                                    Realizziamo Impianti di videosorveglianza per rendere più
                                    sicure le vostre abitazioni e negozi commerciali.
                                </p>
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#condizionamento">
                                   Leggi di più...
                            </button>      
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 wow fadeIn" data-wow-delay=".7s">
                    <div class="cardservizi bg-light">
                        <div class="p-4 text-center services-content">
                            <div class="services-content-icon">
                              
                                <i class="bi bi-bell fa fa-external-link-alt fa-7x mb-4 text-primary"></i>
                                
                                <h4 class="mb-3">Impianti di fotovoltaico</h4>
                                <p class="mb-4">
                                    Realizziamo Impianti di videosorveglianza per rendere più
                                    sicure le vostre abitazioni e negozi commerciali.
                                </p>
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                    data-bs-target="#condizionamento">
                                   Leggi di più...
                            </button>      
                            </div>
                        </div>
                    </div>
                </div>
                
               
                
            </div>
        </div>
    </div>
</x-main-layout>
    <!-- Services End -->
    {{-- start modale impianti elettrici --}}
    <div class="modal fade " id="impianti_elettrici" tabindex="-1" 
         aria-labelledby="impianti_elettrici" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header modale-style">
              <h5 class="modal-title" id="impianti_elettrici">Impianti Elettrici</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal"
                      aria-label="Close">
              </button>
            </div>
            <div class="modal-body modal-fullscreen-md-down modale-style">
              <p class="fs-5">
                Nel corso di quattro decenni abbiamo costantemente ampliato le nostre competenze 
                nel settore elettrico. Dagli impianti civili residenziali siamo passati alla 
                progettazione e realizzazione di quelli destinati all’edilizia prefabbricata e
                 precablata, giungendo poi a soddisfare anche le esigenze del terziario avanzato
                  e dell’industria. L’incessante investimento in innovazione tecnologica, 
                  alimentato dal desiderio di raggiungere traguardi prestazionali di volta in
                   volta più ambiziosi, ci permette di offrire ai nostri clienti soluzioni 
                   sempre all’avanguardia</p>
            </div>
            <div class="modal-footer modale-style">
              <button type="button" class="button_modale" data-bs-dismiss="modal">Close</button>
             
            </div>
          </div>
        </div>
        {{-- end modale --}}

   

    
